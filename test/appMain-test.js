var assert = require('assert')
var fs = require('fs')
var helpers = require('./global-setup')
var path = require('path')
var temp = require('temp').track()

var describe = global.describe
var it = global.it
var beforeEach = global.beforeEach
var afterEach = global.afterEach
var before = global.before
var after = global.after
var expect = require('chai').expect




describe('application loading', function() {
    helpers.setupTimeout(this)

    var app = null
    var tempPath = null

    before(function() {
        tempPath = temp.mkdirSync('spectron-temp-dir-')

        return helpers.startApplication({
            cwd: path.join(__dirname, '..', 'build'),
            args: [
                path.join(__dirname, '..', 'build'),
                '--disableInternalDebug'
            ],
            env: {
                SPECTRON_TEMP_DIR: tempPath
            }
        }).then(function(startedApp) { app = startedApp })
    })

    after(function() {
        return helpers.stopApplication(app)
    })


    it('launches the application', function() {
        return app.client.windowHandles().then(function(response) {
                assert.equal(response.value.length, 1)
            }).waitUntilTextExists('html', 'Pagador')
            .getTitle().should.eventually.equal('Recibos')
    })

    it('passes through args to the launched app', function() {
        return app.mainProcess.argv()
            .should.eventually.contain('--disableInternalDebug')
    })

    describe('browserWindow.capturePage', function() {
        it('returns a Buffer screenshot of the entire page when no rectangle is specified', function() {
            return app.browserWindow.capturePage().then(function(buffer) {
                expect(buffer).to.be.an.instanceof(Buffer)
                expect(buffer.length).to.be.above(0)
            })
        })
    })

    describe('test CPF input', () => {
        it('should ignore non numeric characters', () => {
            return app.client.elements('#cpf').then((elements) => {
                expect(elements.value.length).to.be.equals(2, 'Should have 2 #cpf elements')
                elements.value.forEach((element) => {
                    return app.client.elementIdValue(element.ELEMENT, 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ.')
                })
            }).getValue('#cpf').then(values => {
                values.forEach(value => {
                    expect(value).to.be.equal('');
                })
            })
        })

        it('should format numeric characters', () => {
            return app.client.elements('#cpf').then((elements) => {
                elements.value.forEach((element) => {
                    return app.client.elementIdValue(element.ELEMENT, '00000000000.')
                })
            }).getValue('#cpf').then(values => {
                values.forEach(value => {
                    expect(value).to.be.equal('000.000.000-00');
                })
            })
        })
    })

    describe('test Date input', () => {
        it('should ignore non numeric characters', () => {
            return app.client.elements('#datanasc').then((elements) => {
                expect(elements.value.length).to.be.equals(2, 'Should have 2 #datanasc elements')
                elements.value.forEach((element) => {
                    return app.client.elementIdValue(element.ELEMENT, 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ.')
                })
            }).getValue('#datanasc').then(values => {
                values.forEach(value => {
                    expect(value).to.be.equal('');
                })
            })
        })

        it('should format numeric characters', () => {
            return app.client.elements('#datanasc').then((elements) => {
                elements.value.forEach((element) => {
                    return app.client.elementIdValue(element.ELEMENT, '00000000')
                })
            }).getValue('#datanasc').then(values => {
                values.forEach(value => {
                    expect(value).to.be.equal('00/00/0000');
                })
            })
        })
    })
})