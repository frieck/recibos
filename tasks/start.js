'use strict';

var childProcess = require('child_process');
var npmCmd = require('npm-spawn');
var electron = require('electron');
var gulp = require('gulp');
var utils = require('./utils');

gulp.task('start', ['pre:startProcess'], function() {
    gulp.start('build:watch');
    return startProcess();
});


gulp.task('startProcess', ['pre:startProcess'], function() {
    return startProcess();
});

gulp.task('pre:startProcess', function() {
    if (utils.isProduction())
        return npmCmd(['run', 'build:electron.main'], { cwd: '.' });
    else
        return npmCmd(['run', 'build:electron.main.dev'], { cwd: '.' });
});

function startProcess() {

    return childProcess.spawn(electron, ['./build'], {
            stdio: 'inherit'
        })
        .on('close', function() {
            // User closed the app. Kill the host process.
            process.exit();
        });
}