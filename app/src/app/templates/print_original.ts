export class Template {
public data = `
		<html>
			<head>
				<style>
				body {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12pt;
				font-weight: bold;
				padding: 0px;
				margin: 0px;
				}

				p {
				padding-bottom: 0.4cm;
				}

				.main {
				border: 2px solid black;
				box-shadow: 5px 5px 0px #AAAAAA;
				padding: 5px;
				}

				.title {
				text-transform: uppercase;
				font-size: 24px;
				font-weight: bold;
				}

				.border {
				border-bottom: 1pt solid black;
				font-size: 10pt;
				margin-top: 3px;
				font-weight: normal;
				}

				.nomeLabel {
				float: left;
				}

				.nome {
				float: left;
				width: 12cm;
				}

				.cpfLabel {
				float: left;
				}

				.cpf {
				float: left;
				min-width: 5.2cm;
				}

				.rgLabel {
				float: left;
				}

				.rg {
				float: left;
				width: 7.5cm;
				}

				.dnLabel {
				float: left;
				}

				.data {
				float: left;
				width: 5cm;
				}

				.texto1 {
				float: left;
				}

				.nome2 {
				float: left;
				width: 9cm;
				}

				.cpf2 {
				float: left;
				min-width: 4.2cm;
				}

				.valor {
				float: left;
				min-width: 11.2cm;
				}

				.caixaCheque {
				padding: 2px;
				border: 1px solid black;
				height: 1cm;
				vertical-align: bottom;
				}

				.chequeDiv {
				padding-top: 14px;
				}

				.chequeLabel {
				float: left;
				}

				.cheque {
				float: left;
				min-width: 6.5cm;
				}

				.bancoLabel {
				float: left;
				}

				.banco {
				float: left;
				min-width: 7cm;
				}

				.carimbo {
				margin-top: 1cm;
				border: 1px solid black;
				width: 10cm;
				height: 3cm;
				}

				.assinatura {
				margin-top: 25px;
				float: right;
				min-width: 8cm;
				}

				.btn {
					display: inline-block;
					font-weight: normal;
					line-height: 1.25;
					text-align: center;
					white-space: nowrap;
					vertical-align: middle;
					user-select: none;
					border: 1px solid transparent;
					padding: 0.5rem 1rem;
					font-size: 1rem;
					border-radius: 0.25rem;
					transition: all 0.2s ease-in-out;
				}

				button.btn.btn-success {
					background: #90b900;
					border-color: #90b900;
				}
				
				.btn-danger {
					color: #fff;
					background-color: #d9534f;
					border-color: #d9534f;
				}

				.btn-danger:hover {
					background: #e85656;
					border-color: #d03e3e;
				}

				.btn-success:hover {
					color: #fff;
					background-color: #449d44;
					border-color: #419641;
				}

				.btn:hover {
					background-color: white !important;
					color: #666666;
				}

				.approvePanel {
					position: fixed;
					width: 100%;
					padding: 5px;
					border-bottom: 1px solid black;
					text-align: center;
					background-color: #ffffff;
					z-index: 999;
				  }

				page[size="A4"] {
				position: relative;
				background: white;
				width: 21cm;
				height: 29.7cm;
				display: block;
				margin: 0 auto;
				margin-bottom: 0.5cm;
				padding: 15px;
				top: 50px;
				}
				@media print {
					body, page[size="A4"] {
						margin: 0;
						box-shadow: 0;
						top: 0px;
					}
					.approvePanel{
						display: none;
					}
				
					div{
						page-break-inside: avoid;
					}
				}

				</style>
			</head>
			<body>
				<div class="approvePanel">
					<div class="approveButtons">
						  <button type="button" class="btn btn-success" onclick="printCall()">Dados Verificados</button>
						  <button type="button" class="btn btn-danger" onclick="cancelCall()">Corrigir Dados</button>
					</div>
		  		</div>
				<page size="A4">
					{{#parcelas}}
						<div class="main">
							<p align="center">
							<span class="title">RECIBO</span>
							</p>
							<p align="right">
							<span class="text1">R$</span>
							<span class="valorInteiro border">&nbsp;{{ inteiro }}</span> ,
							<span class="valorDecimal border">&nbsp;{{ centavos }} </span>--
							</p>
							<p>
							<span class="nomeLabel">Recebi de&nbsp;</span>
							<span class="nome border">&nbsp;{{ nome1 }}</span>
							<span class="cpfLabel">&nbsp;CPF&nbsp;</span>
							<span class="cpf border">&nbsp;{{ cpf1 }}</span>
							</p>
							<p>
							<span class="rgLabel">RG&nbsp;</span>
							<span class="rg border">&nbsp;{{ rg1 }}</span>
							<span class="dnLabel">&nbsp;DN&nbsp;</span>
							<span class="data border">&nbsp;{{data1}}</span>
							<span class="texto1">&nbsp;&nbsp;&nbsp;para atendimento médico de</span>
							</p>
							<p>
							<span class="nome2 border">&nbsp;{{ nome2 }}</span>
							<span class="cpfLabel">&nbsp;CPF&nbsp;</span>
							<span class="cpf2 border">&nbsp;{{ cpf2 }}</span>
							<span class="dnLabel">&nbsp;DN&nbsp;</span>
							<span class="data border">&nbsp;{{data2}}</span>
							</p>
							<p>
							<span class="texto1">o valor de&nbsp;</span>
							<span class="valor border">&nbsp;{{ texto }}</span>
							<span class="text1">&nbsp;pelo qual passo o presente recibo.</span>
							</p>
							<div style="padding: 5px">
							<div class="caixaCheque">
								<div class="chequeDiv">
								<span class="chequeLabel">Cheque número&nbsp;</span>
								<span class="cheque border">&nbsp;{{ cheque }}</span>
								<span class="bancoLabel">&nbsp;Banco/agência&nbsp;</span>
								<span class="banco border">&nbsp;{{ banco }}</span>
								</div>
							</div>
							</div>
							<div style="padding: 15px 5px 5px 5px">
							<div style="float: left;">
								<div class="carimbo">
								</div>
							</div>
							<div style="float: right;">
								<span>&nbsp;{{ cidade }}, &nbsp;{{ dia }} de &nbsp;{{ mes }} de  &nbsp;{{ ano }}.</span>
								<br />
								<br />
								<span class="assinatura border">&nbsp;</span>
								<br clear="all" />
								<span style="font-weight: bold; font-size: 10pt; float: right;">Dra. Elaine M.G. Brust - CREMERS 11702</span>
								<br clear="all" />
								<span style="font-weight: normal; font-size: 10pt; float: right;">CPF 378.915.610-87 - CEI 19.045.11341-06</span>
								<br clear="all" />
								<span style="font-weight: normal; font-size: 10pt; float: right;">R. Muck 344/601 - Canoas/RS - 92010-250</span>
							</div>

							</div>
							<br clear="all" />
							<br clear="all" />

						</div>
						<br />
						<br />
						<br />
					{{/parcelas}}
					</page>

					<script>
					function printCall() {
					  var ipcRenderer = require('electron').ipcRenderer;
					  ipcRenderer.send('printRecibo');
					  //window.print();
					}

					function cancelCall() {
						window.close();
					  }
				  </script>
			</body>
		</html>`;
}
