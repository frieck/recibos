import { BrowserWindow, ipcRenderer } from 'electron';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import createNumberMask from 'text-mask-addons/dist/createNumberMask.js';
import * as Mustache from 'mustache';
import { Template } from '../../templates/print_original';

let writtenNumber = require('written-number');

@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.scss'],
  templateUrl: './dashboard.html'
})
export class Dashboard {

  public form: FormGroup;
  public submitted: boolean = false;
  public moneyMask = createNumberMask({
    prefix: '',
    thousandsSeparatorSymbol: '.',
    decimalSymbol: ',',
    // requireDecimal: true,
    allowDecimal: true
  });
  public cpfMask = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  public dataMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public valorTexto;
  public addParcelaText = "Teste...";

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      '_id': [''],
      'cpf1': ['', Validators.compose([
        Validators.required])],
      'nome1': ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(250)])],
      'rg1': ['', Validators.compose([
        Validators.required])],
      'datanasc1': ['', Validators.compose([
        Validators.required])],
      'cpf2': ['', Validators.compose([
        Validators.required])],
      'nome2': ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(250)])],
      'datanasc2': ['', Validators.compose([
        Validators.required])],
      'valores': this.fb.array([this.initValores()]),
      'updatedAt': [''],
      'createdAt': ['']
    });

    this.cancelar();

  }

  public onSubmit(values: any): void {
    this.submitted = true;
    if (this.form.valid) {
      console.log('OK', values);
    } else {
      console.log('NOK', values);
    }

    let t = new Template();
    let computedValues = this.buildValues(values);
    let output = Mustache.render(t.data, computedValues);
    let file = 'data:text/html;charset=UTF-8,' + encodeURIComponent(output);
    ipcRenderer.send('loadRecibo', file);
  }

  public cancelar(): void {
    this.form.reset();
    this.valorTexto = '';
    for (let i = this.form.controls['valores']['controls'].length; i >= 1; i--) {
      this.removeParcela(i);
    }
    this.form.controls['valores']['controls'][0].controls.vencimento.setValue(this.getVencimentoToday());
  }

  getVencimentoToday(): string {
    let d = new Date();
    return d.getDate() + '/' + (d.getMonth() < 10 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1)) + '/' + d.getFullYear();
  }

  addParcela() {
    const control = <FormArray>this.form.controls['valores'];
    control.push(this.initValores());
  }

  removeParcela(i: number) {
    const control = <FormArray>this.form.controls['valores'];
    control.removeAt(i);
  }

  copiaPagador() {
    this.form.controls['cpf2'].setValue(this.form.controls['cpf1'].value);
    this.form.controls['nome2'].setValue(this.form.controls['nome1'].value);
    this.form.controls['datanasc2'].setValue(this.form.controls['datanasc1'].value);
  }

  private buildValues(v: any): void {
    let values: any = {};
    // Pagador
    values.cpf1 = v.cpf1;
    values.nome1 = v.nome1;
    values.data1 = v.datanasc1;
    values.rg1 = v.rg1;
    // Paciente
    values.cpf2 = v.cpf2;
    values.nome2 = v.nome2;
    values.data2 = v.datanasc2;

    values.parcelas = [];
    for (let i = 0; i < v.valores.length; i++) {
      let valor = v.valores[i].valor.replace(/\./g, '').split(',');
      values.parcelas[i] = {};
      values.parcelas[i].vencimento = v.valores[i].vencimento;
      values.parcelas[i].inteiro = valor[0];
      if (valor.length > 1 && parseInt(valor[1], 10) > 0) {
        if (valor[1].length < 2) {
          valor[1] += '0';
        }
        values.parcelas[i].centavos = valor[1];
      } else {
        values.parcelas[i].centavos = '00';
      }
      values.parcelas[i].texto = this.porExtenso(v.valores[i].valor);
      let dVencimento = v.valores[i].vencimento.split('/');

      values.parcelas[i].dia = dVencimento[0];
      values.parcelas[i].mes = this.getMes(parseInt(dVencimento[1], 10));
      values.parcelas[i].ano = dVencimento[2];
    }


    values.cidade = 'Canoas/RS';
    let d = new Date();


    console.log(values);

    return values;
  }

  private getMes(mes: number): string {
    let meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];
    return meses[mes-1];
  }

  private porExtenso(valor): String {
    let val = valor.replace(/\./g, '');
    let v = val.split(',');
    if (v[0] === '' || parseInt(v[0], 10) <= 0) {
      return '';
    }
    let ret = writtenNumber(v[0], { lang: 'pt' }) + ' reais';
    if (v.length > 1 && parseInt(v[1], 10) > 0) {
      if (v[1].length < 2) {
        v[1] += '0';
      }
      ret += ' e ' + writtenNumber(v[1], { lang: 'pt' }) + ' centavos';
    }

    return ret;
  }

  private initValores() {
    return this.fb.group({
      'valor': ['', Validators.compose([Validators.required])],
      'vencimento': ['', Validators.compose([Validators.required])]
    });
  }
}
