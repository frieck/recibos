import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgaModule } from '../../theme/nga.module';

import { Dashboard } from './dashboard.component';
import { routing } from './dashboard.routing';

import { TooltipModule } from 'ngx-bootstrap';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
    TextMaskModule,
    TooltipModule.forRoot(),
  ],
  declarations: [
    Dashboard
  ],
  providers: [
  ]
})
export class DashboardModule { }
