import { Component } from '@angular/core';
import { UploadInput } from 'ngx-uploader';

@Component({
  selector: 'layouts',
  templateUrl: './layouts.html',
})
export class Layouts {

  public defaultPicture = 'assets/img/theme/no-photo.png';
  public profile: any = {
    picture: 'assets/img/app/profile/profile.png'
  };
  public uploaderOptions: UploadInput = {
    type: 'uploadAll',
    url: '',
  };

  constructor() {
  }

  ngOnInit() {
  }
}
