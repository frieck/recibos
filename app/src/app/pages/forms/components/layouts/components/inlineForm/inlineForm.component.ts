import { Component } from '@angular/core';

@Component({
  selector: 'inline-form',
  styleUrls: ['./inlineForm.scss'],
  templateUrl: './inlineForm.html',
})
export class InlineForm {

  isRemember: boolean = false;

  constructor() {
  }
}
