import { Component, Input } from '@angular/core';

@Component({
  selector: 'ba-card-actions',
  templateUrl: './baCardActions.html',
})
export class BaCardActions {
  @Input() title: String;
  @Input() baCardClass: String;
  @Input() cardType: String;
}
