import mongoose from 'mongoose';
import CPF from 'gerador-validador-cp';

// Create a `schema` for the `Usuario` object
let usuarioSchema = new mongoose.Schema({
    CPF: {
        type: String,
        unique: true,
        required: [true, 'CPF é um campo obrigatório!'],
        minlength: [11, 'CPF deve possuir 11 caracteres!'],
        maxlength: [11, 'CPF deve possuir 11 caracteres!'],
        validate: {
            validator: function(v) {
                return CPF.validate(v);
            },
            message: '{VALUE} não é um código CPF válido!'
        },
    },
    nome: {
        type: String,
        minlength: [3, 'Nome deve possuir um mínimo de 3 caracteres!'],
        maxlength: [250, 'Nome deve possuir um máximo de 250 caracteres!']
    }
}, {
    timestamps: true
});


// Expose the model so that it can be imported and used in
// the controller (to search, delete, etc.)
export default mongoose.model('tipoProduto', usuarioSchema);
